package com.tron.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.tron.Tron;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Tron.WIDTH;
		config.height = Tron.HEIGHT;
		config.title = Tron.TITLE;
		new LwjglApplication(new Tron(), config);
	}
}
