# Walka na arenie z motywem przewodnim TRON #

### Harmonogram projektu

## 1 – Stworzenie mechaniki gry i wygląd
- ** Tworzenie menu z miejscem na logowanie - #1 (08.03.2016)**
- ** Game Loop (Game State Manager) - #1 (08.03.2016)**
- ** Kolizje wojowników z brzegami areny, skrzynkami oraz łapanie dysku - #2 (15.03.2016)**
- ** Animacje - #9 (17.05.2016) **
## 2 - Kolizje
- ** Lot dysku oraz jego kolizja z obrzeżami areny - #2 (15.03.2016)**
- ** Kolizja z dyskiem przeciwnika – #3 (22.03.2016) **
- ** Generowanie losowego ustawienia skrzynek - #3 (22.03.2016) **
- ** Kolizja dysku ze skrzynkami - #4 (05.04.2016) **
## 3 – Logika serwera
- ** Inicjalizacja logiki serwera (oparcie połączenia na socketach) - #4 (05.04.2016) **
- ** Dopracowanie logiki serwera oraz klienta, inicjalizacja logiki przesyłania danych - #6 (19.04.2016) **
- ** Logika komunikacji serwer-klient (przesyłanie róznych ramek w zależności od potrzeb),
  tworzenie tego samego ustawienia skrzyń przy rozpoczęciu - #6 (19.04.2016) **
- ** Przesyłanie wektora położenia wojowników oraz zderzeń dysku - #7 (26.04.2016) **
## 4 – Współbieżność
-  ** Osobne wątki na sockecie - wielu podłączonych graczy na sockecie - #8 (10.05.2016) **
-  ** Zaprojektowanie sztucznej inteligencji dla przeciwnika w wypadku gry offline -#11 (31.05.2016) **
-  ** Zapewnienie bezpieczeństwa współbieżności - #12 (01.06.2016) **
- ** Korzystanie z synchronizacji zapewnionej przez bibliotekę libGDX (np. usuwanie zasobów) - #12 (01.06.2016) **
## 5 – Baza Danych
- ** Inicjalizacja bazy danych, podstawowa funkcjonalność - #5 (12.04.2016) **
- ** Przechowywanie danych gracza, podstawowe informacje o koncie - #5 (12.04.2016) **
- ** Struktura rankingowa graczy - #8 (10.05.2016) **
- ** Logowanie do bazy danych - #7 (26.04.2016) **
## 6 - Operacje na plikach
- ** Wczytywanie muzyki - #10 (24.05.2016) **
- ** Wczytywanie animacji ze spirte'ów, spritebatch'ów -#9 (26.04.2016) **
- ** Zapis zasobów do pliku zip -#11 (31.05.2016) **
- ** Wczytywanie grafik, sprite'ow (Wirtualny system plikow) - #10 (24.05.2016) **