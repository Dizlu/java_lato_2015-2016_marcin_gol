package com.tron.common;

/**
 * Created by Marcin on 06.04.2016.
 */
public class FrameType {
    public static final int LOGIN_FRAME = 1;
    public static final int LOGOUT_FRAME = 2;
    public static final int MOVEMENT_FRAME = 3;
    public static final int THROW_FRAME = 4;
    public static final int MESSAGE_FRAME = 5;
    public static final int BOX_FRAME = 6;
}
