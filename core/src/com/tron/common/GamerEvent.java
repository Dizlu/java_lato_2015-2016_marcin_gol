package com.tron.common;

import com.badlogic.gdx.math.Vector2;
import com.tron.sprites.Fighter;

/**
 * Created by Marcin on 06.04.2016.
 */
public interface GamerEvent {
    public void connectionError();
    public void userLogged(int id, String login, float x, float y);
    public void userLeft(int id);
    public void movementReceived (int userId, Fighter fighter, float x, float y);
    public void messageReceived( int userId, String userMessage);
    public void throwReceived(int userId, float x, float y);
}
