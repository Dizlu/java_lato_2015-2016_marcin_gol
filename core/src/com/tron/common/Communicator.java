package com.tron.common;

import com.badlogic.gdx.utils.Array;
import com.tron.sprites.Box;

/**
 * Created by Marcin on 06.04.2016.
 */
public interface Communicator {

    public int getUserId();
    public String getUserLogin();

    public void setEvent(GamerEvent event);

    public boolean sendMovementFrame(int movementType);
    public boolean sendMessageFrame(String message);
    public boolean sendBoxesFrame(Array<Box> boxes);

    public boolean sendThrowFrame();
}
