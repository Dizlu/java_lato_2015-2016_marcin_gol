package com.tron.javadb;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Marcin on 10.04.2016.
 */
public class FighterDB {
    public int id;
    private String login;
    private int positionx, positiony;
    private int velocityx, velocityy;
    private String fighter;
    private int rank;
    private String password;

    public FighterDB(int id, String login, String password, int positionx, int positiony, int velocityx, int velocityy, String fighter, int rank){
        this.id = id;
        this.login = login;
        this.password = password;
        this.positionx = positionx;
        this.positiony = positiony;
        this.velocityx = velocityx;
        this.velocityy = velocityy;
        this.fighter = fighter;
        this.rank = rank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPositionx() {
        return positionx;
    }

    public void setPositionx(int positionx) {
        this.positionx = positionx;
    }

    public int getPositiony() {
        return positiony;
    }

    public void setPositiony(int positiony) {
        this.positiony = positiony;
    }

    public int getVelocityx() {
        return velocityx;
    }

    public void setVelocityx(int velocityx) {
        this.velocityx = velocityx;
    }

    public int getVelocityy() {
        return velocityy;
    }

    public void setVelocityy(int velocityy) {
        this.velocityy = velocityy;
    }

    public String getFighter() {
        return fighter;
    }

    public void setFighter(String fighter) {
        this.fighter = fighter;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
