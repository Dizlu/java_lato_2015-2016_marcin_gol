package com.tron.javadb;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JavaDB {

    public JavaDB() {
        String baza = "fighters";
        Connection polaczenie = polacz(baza);
        stworzTablice(polaczenie, baza);


        /*FighterDB fighter1 = new FighterDB(1, "My name", 30, 40, 10, 3, "fighter.png");
        dodajDane(fighter1, baza);
        FighterDB fighter2 = new FighterDB(3, "My name", 310, 41230, 3210, 33, "fighter.png");
        dodajDane(fighter2, baza);
        FighterDB fighter3 = new FighterDB(2, "My nam3", 3, 4, 1, 3, "fighter.png");
        dodajDane(fighter3, baza);
        fighter3.setFighter("master.png");

        szukaj(baza, "LOGIN", "My name");
        szukaj(baza, "ID", "2");



        szukaj(baza, "LOGIN", "My name");
        zmien(baza, "LOGIN", "My name", "FIGHTER", "fighter!!!!.png");
        szukaj(baza, "LOGIN", "My name");


        usun(baza, "LOGIN", "My nam3");
        szukaj(baza, "LOGIN", "My nam3");*/
    }



    /**
     * Metoda odpowiedzialna za połączenie z bazą
     * jeśli bazy nie ma to zostaje utworzona
     */
    public static Connection polacz(String baza) {
        Connection polaczenie = null;
        try {
            // Wskazanie jaki rodzaj bazy danych będzie wykorzystany, tu sqlite
            Class.forName("org.sqlite.JDBC");
            // Połączenie, wskazujemy rodzaj bazy i jej nazwę
            polaczenie = DriverManager.getConnection("jdbc:sqlite:"+baza+".db");
            System.out.println("Połączyłem się z bazą "+baza);
        } catch (Exception e) {
            System.err.println("Błąd w połączeniu z bazą: \n" + e.getMessage());
            return null;
        }
        return polaczenie;
    }

    public static void stworzTablice(Connection polaczenie, String tablica) {
        // Obiekt odpowiadający za wykonanie instrukcji
        Statement stat = null;
        try {
            stat = polaczenie.createStatement();
            // polecenie SQL tworzące tablicę
            String tablicaSQL = "CREATE TABLE " + tablica
                    + " (ID                   INT PRIMARY KEY NOT NULL,"
                    + " LOGIN                 CHAR(50)    NOT NULL, "
                    + " PASSWORD              CHAR(50)    NOT NULL, "
                    + " POSITIONX             INT, "
                    + " POSITIONY             INT, "
                    + " VELOCITYX             INT, "
                    + " VELOCITYY             INT, "
                    + " FIGHTER               CHAR(50) NOT NULL,"
                    + " RANK                  INT) ";

            // wywołanie polecenia
            stat.executeUpdate(tablicaSQL);
            // zamykanie wywołania i połączenia
            stat.close();
            polaczenie.close();
        } catch (SQLException e) {
            System.out.println("Nie mogę stworzyć tablicy" + e.getMessage());
        }
    }

    public static void dodajDane(FighterDB fighterDB, String baza) {
        Connection polaczenie = null;
        Statement stat = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");

            stat = polaczenie.createStatement();
            String dodajSQL = "INSERT INTO " + baza
                    + " (ID, LOGIN, PASSWORD, POSITIONX, POSITIONY, VELOCITYX, VELOCITYY, FIGHTER, RANK) "
                    + "VALUES ("
                    + fighterDB.getId() + ","
                    + "'" + fighterDB.getLogin() + "',"
                    + "'" + fighterDB.getPassword() + "',"
                    + fighterDB.getPositionx() + ","
                    + fighterDB.getPositiony() + ","
                    + fighterDB.getVelocityx() + ","
                    + fighterDB.getVelocityy() + ","
                    + "'" + fighterDB.getFighter() + "',"
                    + fighterDB.getRank() + "  );";

            stat.executeUpdate(dodajSQL);
            stat.close();
            polaczenie.close();
            // Komunikat i wydrukowanie końcowej formy polecenia SQL
            System.out.println("Polecenie: \n" + dodajSQL + "\n wykonane.");
        } catch (Exception e) {
            System.out.println("Nie mogę dodać danych " + e.getMessage());
        }
    }




    public static void szukaj(String baza, String pole, String wartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie wyszukania
            String szukajSQL = "SELECT * FROM " + baza
                    + " WHERE " + pole + "=='" + wartosc + "';";

            ResultSet wynik = stat.executeQuery(szukajSQL);
            System.out.println("Wynik polecenia:\n" + szukajSQL);

            while (wynik.next()) {
                int id = wynik.getInt("id");
                System.out.println("ID:       " + id);
                System.out.println("Login:   " + wynik.getString("login"));
                System.out.println("Positionx:  " + wynik.getString("positionx"));
                System.out.println("Positiony:  " + wynik.getString("positiony"));
                System.out.println("Velocityx:  " + wynik.getString("velocityx"));
                System.out.println("Velocityy:  " + wynik.getString("velocityy"));
                System.out.println("Fighter:    " + wynik.getString("fighter"));
                System.out.println("Rank:       " + wynik.getString("rank"));
                System.out.println(" ---------------------- ");
            }

            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę wyszukać danych " + e.getMessage());
        }

    }

    public static Map fetch(String baza, String pole, String jak) {
        Connection polaczenie = null;
        Statement stat = null;
        Map m = new HashMap();
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie wyszukania
            String szukajSQL = "SELECT * FROM " + baza
                    + " ORDER BY " + pole + " " + jak;

            ResultSet wynik = stat.executeQuery(szukajSQL);
            System.out.println("Wynik polecenia:\n" + szukajSQL);

            while (wynik.next()) {
                int id = wynik.getInt("id");
                System.out.println("ID:       " + id);
                System.out.println("Login:   " + wynik.getString("login"));
                System.out.println("Password:   " + wynik.getString("password"));
                System.out.println("Positionx:  " + wynik.getString("positionx"));
                System.out.println("Positiony:  " + wynik.getString("positiony"));
                System.out.println("Velocityx:  " + wynik.getString("velocityx"));
                System.out.println("Velocityy:  " + wynik.getString("velocityy"));
                System.out.println("Fighter:    " + wynik.getString("fighter"));
                System.out.println("Rank:    " + wynik.getString("rank"));
                System.out.println(" ---------------------- ");
                m.put(wynik.getString("login"), wynik.getString("rank"));
            }

            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę wyszukać danych " + e.getMessage());
        }
        return m;
    }

    public static String szukajPrzynies(String baza, String pole, String wartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie wyszukania
            String szukajSQL = "SELECT * FROM " + baza
                    + " WHERE " + pole + "=='" + wartosc + "';";

            ResultSet wynik = stat.executeQuery(szukajSQL);
            System.out.println("Wynik polecenia:\n" + szukajSQL);

            String temp = wynik.getString("login");
            System.out.println("Returning " + temp + " from a database.");

            wynik.close();
            stat.close();
            polaczenie.close();
            return temp;
        } catch (Exception e) {
            System.out.println("Nie mogę wyszukać danych " + e.getMessage());
        }
        return null;
    }

    public static String szukajPrzyniesRank(String baza, String pole, String wartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        String temp = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie wyszukania
            String szukajSQL = "SELECT * FROM " + baza
                    + " WHERE " + pole + "=='" + wartosc + "';";

            ResultSet wynik = stat.executeQuery(szukajSQL);
            System.out.println("Wynik polecenia:\n" + szukajSQL);

            temp = wynik.getString("rank");

            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę wyszukać danych " + e.getMessage());
        }
        return temp;
    }

    public static String szukajPrzyniesPassword(String baza, String pole, String wartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        String temp = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie wyszukania
            String szukajSQL = "SELECT * FROM " + baza
                    + " WHERE " + pole + "=='" + wartosc + "';";

            ResultSet wynik = stat.executeQuery(szukajSQL);
            System.out.println("Wynik polecenia:\n" + szukajSQL);

            temp = wynik.getString("password");


            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę wyszukać danych " + e.getMessage());
        }
        System.out.println("Jego haslo to: " + temp + " !!!!!!!!!!!!!!");
        return temp;
    }

    public static void zmien(String baza, String poleSzukane, String wartoscSzukana,
                             String poleZmieniane, String nowaWartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            // Polecenie zmiany
            String zmienSQL = "UPDATE " + baza + " SET "
                    + poleZmieniane + " = '" + nowaWartosc
                    + "' WHERE " + poleSzukane + "=='" + wartoscSzukana + "';";

            ResultSet wynik = stat.executeQuery(zmienSQL);
            System.out.println("Wynik polecenia:\n" + zmienSQL);
            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę poprawić danych " + e.getMessage());
        }

    }

    public static void usun(String baza, String pole, String wartosc) {
        Connection polaczenie = null;
        Statement stat = null;
        try {
            Class.forName("org.sqlite.JDBC");
            polaczenie = DriverManager.getConnection("jdbc:sqlite:" + baza + ".db");
            stat = polaczenie.createStatement();
            String usunSQL = "DELETE FROM "+ baza + " WHERE " + pole +
                    "='" + wartosc + "';";
            System.out.println("Polecenie:\n" + usunSQL);
            ResultSet wynik = stat.executeQuery(usunSQL);

            wynik.close();
            stat.close();
            polaczenie.close();
        } catch (Exception e) {
            System.out.println("Nie mogę usunąć danych " + e.getMessage());
        }

    }
}