package com.tron.socketApp;

import com.badlogic.gdx.utils.Array;
import com.tron.common.Communicator;
import com.tron.common.FrameType;
import com.tron.common.GamerEvent;
import com.tron.sprites.Box;
import com.tron.sprites.Fighter;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Diylu on 31.03.2016.
 */
public class Server implements Communicator {
    private Object locker = new Object();
    private boolean connected = false;
    private boolean looped = false;
    private GamerEvent event = null;
    private Thread thread;
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;


    private Fighter serverFighter = null;
    private Fighter clientFighter = null;
    private Array<Box> boxes = null;


    @Override
    public void setEvent (GamerEvent event){
        this.event = event;
    }
    public void setServerFighter(Fighter fighter){
        serverFighter = fighter;
        serverFighter.id = fighter.id;
    }
    public void setClientFighter(Fighter fighter){
        clientFighter = fighter;
        clientFighter.id = fighter.id;
    }
    public void setBoxes(Array<Box> boxes){
        this.boxes = boxes;
    }
    public void setConnected(boolean connected) {
        this.connected = connected;
    }
    public int getUserId(){
        return this.serverFighter.getId();
    }
    public String getUserLogin() {
        return this.serverFighter.getLogin();
    }
    public boolean isConnected() {
        return connected;
    }
    public boolean start(int port, String login){
        if (this.connected) return false;

        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e){
            return false;
        }

        this.serverFighter = new Fighter(1, login, 0, 0);
        this.clientFighter = new Fighter(2, login, 20, 20);

        this.thread = new Thread ( () -> {
           while (this.looped){
               System.out.println("looking for client..");
               try {
                   this.clientSocket = this.serverSocket.accept();

                   this.inputStream = new DataInputStream (this.clientSocket.getInputStream());
                   this.outputStream = new DataOutputStream (this.clientSocket.getOutputStream());

                   System.out.println("Loop in server");
                   Byte temp = inputStream.readByte();
                   System.out.println(temp);
                   outputStream.writeByte(temp);
                   String tempUTF = inputStream.readUTF();
                   System.out.println(tempUTF);
                   outputStream.writeUTF(tempUTF);

                   while (looped && this.makeIteration());      // How to make this not wait another socket?
               } catch (IOException e){
                   this.event.userLeft(this.clientFighter.getId());
               } finally {
                   this.closeClient();
               }
           }
        });


        this.looped = true;
        this.thread.setName("Client thread");
        this.thread.start();
        this.connected = true;

        this.event.userLogged(this.serverFighter.getId(), this.serverFighter.getLogin(), this.serverFighter.getPosition().x, this.serverFighter.getPosition().y);

        return true;
    }

    private boolean makeIteration() throws IOException {
        int frameType = this.inputStream.readByte();
        //System.out.println("Receiving frame... got frame: " + frameType);
        switch (frameType){
            case FrameType.LOGIN_FRAME:
                return this.interpretLoginFrame();
            case FrameType.LOGOUT_FRAME:
                return this.interpretLogoutFrame();
            case FrameType.MESSAGE_FRAME:
                return this.interpretMessageFrame();
            case FrameType.MOVEMENT_FRAME:
                return this.interpretMovementFrame();
            case FrameType.THROW_FRAME:
                return this.interpretThrowFrame();
            case FrameType.BOX_FRAME:
                return this.interpretBoxesFrame(boxes);
            default:
                return false;
        }
    }

    private boolean interpretBoxesFrame(Array<Box> boxes) {
        if (this.connected == false) return false;
        try {
            for (int i = 0; i < 6; i++){
                Float firstBoxPos = this.inputStream.readFloat(), secondBoxPos = this.inputStream.readFloat();
                boxes.get(i).getPosBox().x = firstBoxPos;
                boxes.get(i).getPosBox().y = secondBoxPos;

                boxes.get(i).getBounds().x = firstBoxPos;
                boxes.get(i).getBounds().y = secondBoxPos;

                boxes.get(i).updateBounds();

                System.out.println("Moved a box!");
            }
        } catch (IOException e){
            return false;
        }
        return true;
    }

    public void disconnect(){
        if (!this.connected){
            return;
        }
        this.looped = false;
        //safe
        this.closeObjects();
        this.joinThread();

        this.connected = false;

        this.event.userLeft(this.serverFighter.getId());
    }

    private boolean joinThread() {
        try {
            //safe
            this.thread.join();
            return true;
        } catch (InterruptedException e){
            return false;
        }
    }

    //safe
    private synchronized void closeObjects() {
        this.closeClient();
        this.closeObject(this.serverSocket);
    }

    public boolean sendBoxesFrame (Array<Box> boxes){
        return true;
    }

    @Override
    public boolean sendMessageFrame (String message){
        if (this.connected == false){
            return false;
        }
        try {//safe
            synchronized(this.locker){
                if (this.outputStream != null) {
                    this.outputStream.writeByte(FrameType.MESSAGE_FRAME);
                    this.outputStream.writeInt(this.serverFighter.getId());
                    this.outputStream.writeUTF(message);
                }
            }
            this.event.messageReceived(this.serverFighter.getId(), message);
        } catch (IOException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean sendMovementFrame( int movementType ){
        if (this.connected == false){
            return false;
        }
        try {
            //safe
            synchronized (this.locker){
                if(this.outputStream != null){
                    this.outputStream.writeByte(FrameType.MOVEMENT_FRAME);
                    this.outputStream.writeByte(movementType);
                    /*this.outputStream.writeFloat(this.serverFighter.getPosition().x);
                    this.outputStream.writeFloat(this.serverFighter.getPosition().y);*/
                }
            }
            //this.event.movementReceived(this.serverFighter.getId(), this.serverFighter.getPosition().x, this.serverFighter.getPosition().y);
        } catch (IOException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean sendThrowFrame(){
        if (this.connected == false){
            return false;
        }
        try {
            //safe
            synchronized (this.locker){
                if(this.outputStream != null){
                    this.outputStream.writeByte(FrameType.THROW_FRAME);
                }
            }
            this.event.throwReceived(this.serverFighter.getId(), this.serverFighter.getPosition().x, this.serverFighter.getPosition().y);
        } catch (IOException e){
            return false;
        }
        return true;
    }

    private boolean interpretThrowFrame() throws IOException{
        this.clientFighter.disc.toggleThrow();

        this.event.movementReceived(this.clientFighter.getId(), this.clientFighter, this.clientFighter.getPosition().x
                , this.clientFighter.getPosition().y);
        return true;
    }

    private boolean interpretMovementFrame() throws IOException{
        Byte temp = null;
        //safe
        synchronized (this.locker) {
            temp = this.inputStream.readByte();
        }
        this.clientFighter.move(temp);
        System.out.println(temp);

        /*synchronized(this.locker){
            this.outputStream.writeByte(FrameType.MOVEMENT_FRAME);
            this.outputStream.writeByte(temp);
            this.outputStream.writeFloat(this.clientFighter.getPosition().x);
            this.outputStream.writeFloat(this.clientFighter.getPosition().y);
        }*/

        this.event.movementReceived(this.clientFighter.getId(), this.clientFighter, this.clientFighter.getPosition().x
                , this.clientFighter.getPosition().y);
        return true;
    }

    private boolean interpretMessageFrame() throws IOException{
        int login = this.inputStream.readInt();
        String message = this.inputStream.readUTF();
        //safe
        synchronized(this.locker){
            this.outputStream.writeByte(FrameType.MESSAGE_FRAME);
            this.outputStream.writeInt(this.clientFighter.getId());
            this.outputStream.writeUTF(message);
        }
        this.event.messageReceived(login, message);
        return true;
    }

    private boolean interpretLoginFrame() throws IOException{
        String login = this.inputStream.readUTF();

        //safe
        synchronized (this.locker){
            if (login.equals( this.serverFighter.getLogin())){
                this.outputStream.writeBoolean(false);

                return false;
            }

            // Answer when client tries to login
            this.outputStream.writeBoolean(true);
            this.outputStream.writeInt(this.clientFighter.getId());
            this.outputStream.writeFloat(this.clientFighter.getPosition().x);
            this.outputStream.writeFloat(this.clientFighter.getPosition().y);

            // After login communication
            this.outputStream.writeByte(FrameType.LOGIN_FRAME);
            this.outputStream.writeInt(this.serverFighter.getId());
            this.outputStream.writeUTF(this.serverFighter.getLogin());
            this.outputStream.writeFloat(this.serverFighter.getPosition().x);
            this.outputStream.writeFloat(this.serverFighter.getPosition().y);
        }
        this.event.userLogged(this.clientFighter.getId(), this.clientFighter.getLogin()
            , this.clientFighter.getPosition().x, this.clientFighter.getPosition().y);

        return true;
    }

    private boolean interpretLogoutFrame() throws IOException {
        //this.event.userLeft( this.clientFighter.getId() );
        //this.clientFighter = null;
        //safe
        synchronized(this.locker) {
            this.closeClient();
            this.connected = false;
        }
        return true;
    }

    private boolean closeObject(Closeable obj){
        if (obj == null){
            return false;
        }
        try {
            obj.close();
            return true;
        } catch (IOException e){
            return false;
        }
    }
    private void closeClient(){
        this.closeObject(this.outputStream);
        this.closeObject(this.inputStream);
        this.closeObject(this.clientSocket);

        this.clientFighter = null;
    }
    public void stop(){
        this.looped = false;

        this.closeObjects();
        this.joinThread();

        this.connected = false;
    }

}
