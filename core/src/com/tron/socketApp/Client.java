package com.tron.socketApp;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.tron.common.Communicator;
import com.tron.common.FrameType;
import com.tron.common.GamerEvent;
import com.tron.sprites.Box;
import com.tron.sprites.Fighter;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Diylu on 01.04.2016.
 */
public class Client implements Communicator {
    private Object locker = new Object();
    private boolean connected = false;
    private boolean looped = false;

    private GamerEvent event;

    private int userId;
    private String userLogin;

    private Fighter serverFighter = null;
    private Fighter clientFighter = null;

    private Socket socket = null;
    private Thread thread = null;

    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;

    @Override
    public int getUserId()
    {
        return this.userId;
    }

    @Override
    public String getUserLogin()
    {
        return this.userLogin;
    }

    @Override
    public void setEvent( GamerEvent event )
    {
        this.event = event;
    }

    public boolean connect( String host, int port, String login ) {
        if ( this.connected )
            return false;
        try {
            this.socket = new Socket( host, port );
            this.inputStream = new DataInputStream( this.socket.getInputStream() );
            this.outputStream = new DataOutputStream( this.socket.getOutputStream() );

            this.outputStream.writeByte( FrameType.LOGIN_FRAME );
            System.out.println("Server gave us response: " + inputStream.readByte());
            this.outputStream.writeUTF( login );
            System.out.println("Server gave us response: " + inputStream.readUTF());


            connected = true;
            this.runThread();

            //Login event frame?
            return true;
        }
        catch ( IOException e ) {
            this.closeObjects();
            System.out.println("Error: couldn't connect to server!");
        }
        return false;
    }

    public void disconnect() {
        if (this.connected == false) return;

        this.looped = false;
        this.closeObjects();
        this.joinThread(); //Causing an error when ending StackState

        this.event.userLeft(clientFighter.getId());
        this.connected = false;
    }

    public void setServerFighter(Fighter fighter){
        this.serverFighter = fighter;
    }
    public void setClientFighter(Fighter fighter){
        this.clientFighter = fighter;
    }


    @Override
    public boolean sendMovementFrame(int movementType) {
        if (this.connected == false) return false;
        try {
            //safe
            synchronized (this.locker) {
                this.outputStream.writeByte(FrameType.MOVEMENT_FRAME);
                this.outputStream.writeByte(movementType);
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendBoxesFrame(Array<Box> boxes){
        if (this.connected == false) return false;
        try {
            this.outputStream.writeByte(6);
            for (Box box: boxes){
                this.outputStream.writeFloat(box.getPosBox().x);
                this.outputStream.writeFloat(box.getPosBox().y);
            }
        } catch (IOException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean sendMessageFrame(String message) {
        if (this.connected == false) return false;
        try {
            this.outputStream.writeByte(FrameType.MESSAGE_FRAME);
            this.outputStream.writeInt(this.userId);
            this.outputStream.writeUTF(message);
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendThrowFrame(){
        if (this.connected == false){
            return false;
        }
        try {
            //safe
            synchronized (this.locker){
                if(this.outputStream != null){
                    this.outputStream.writeByte(FrameType.THROW_FRAME);
                }
            }
            this.event.throwReceived(this.clientFighter.getId(), this.clientFighter.getPosition().x, this.clientFighter.getPosition().y);
        } catch (IOException e){
            return false;
        }
        return true;
    }

    private boolean interpretThrowFrame() throws IOException{
        this.serverFighter.disc.toggleThrow();

        this.event.movementReceived(this.serverFighter.getId(), this.serverFighter, this.serverFighter.getPosition().x
                , this.serverFighter.getPosition().y);
        return true;
    }

    private void interpretLoginFrame() throws IOException {
        int userId = this.inputStream.readInt();
        String userLogin = this.inputStream.readUTF();

        float userX = this.inputStream.readFloat();
        float userY = this.inputStream.readFloat();

        clientFighter.getPosition().x = userX;
        clientFighter.getPosition().y = userY;
        this.event.userLogged( userId, userLogin, userX, userY );
    }

    public void sendLogoutFrame() throws IOException {
        this.outputStream.writeByte(FrameType.LOGOUT_FRAME);
        this.disconnect();
    }

    private void interpretLogoutFrame() throws IOException {
        int userId;
        //safe
                synchronized(this.locker){
                    userId = this.inputStream.readInt();
                }
        this.event.userLeft(userId);
    }

    private void interpretMovementFrame() throws IOException {
        int movementType;
        //safe
        synchronized(this.locker) {
            movementType = this.inputStream.readByte();
            this.serverFighter.move(movementType);
        }
        this.event.movementReceived( userId, serverFighter, serverFighter.getPosition().x, serverFighter.getPosition().y);

    }

    private void interpretMessageFrame() throws IOException {
        int userId;
        String userMessage;
        //safe
        synchronized(this.locker) {
            userId = this.inputStream.readInt();
            userMessage = this.inputStream.readUTF();
        }

        this.event.messageReceived( userId, userMessage );
    }

    public void makeIteration() throws IOException {
        int frameType = this.inputStream.readByte();
        switch ( frameType ) {
            case FrameType.LOGIN_FRAME:
                this.interpretLoginFrame();
                break;
            case FrameType.LOGOUT_FRAME:
                this.interpretLogoutFrame();
                break;
            case FrameType.MOVEMENT_FRAME:
                this.interpretMovementFrame();
                break;
            case FrameType.MESSAGE_FRAME:
                this.interpretMessageFrame();
                break;
            case FrameType.THROW_FRAME:
                this.interpretThrowFrame();
                break;
            default:
                break;
        }
    }

    public void runThread() {
        looped = true;
        //safe
        this.thread = new Thread(() -> {
            boolean error = false;
            try {
                while (this.looped)
                    this.makeIteration();
            }
            catch ( IOException e ) {
                error = true;
            }
            finally {
                //this.closeObjects();

                //this.looped = false;
                //this.connected = false;

                //if (error)
                //    this.event.connectionError();
            }
        });

        this.thread.setName( "Client thread" );
        this.thread.start();
    }

    private boolean joinThread() {
        try {
            this.thread.join();

            return true;
        }
        catch ( InterruptedException e ) {
            return false;
        }
    }

    private boolean closeObject( Closeable object ) {
        if ( object == null )
            return false;
        try {
            object.close();
            return true;
        }
        catch ( IOException e ) {
            return false;
        }
    }

    //safe
    private synchronized void closeObjects() {
        this.closeObject( this.outputStream );
        this.closeObject( this.inputStream );
        this.closeObject( this.socket );
    }

    public boolean isConnected() {
        return connected;
    }
}
