package com.tron.utils;

import com.tron.common.Communicator;
import com.tron.common.GamerEvent;
import com.tron.sprites.Fighter;
import com.tron.states.GameStateManager;
import com.tron.states.ServerState;
import com.tron.states.State;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Marcin on 07.04.2016.
 */
public class EUtil {

    public static void init(final String frameName, final Communicator communicator) {

        communicator.setEvent(new GamerEvent()
        {
            @Override
            public void userLogged(int id, String login, float x, float y) {
                System.out.println("User logged! id: " + id + " login: " + login);
            }

            @Override
            public void userLeft(int id) {
                System.out.println("User left! id: " + id);
            }

            @Override
            public void movementReceived(int userId, Fighter fighter, float userX, float userY) {
                fighter.getPosition().x = userX;
                fighter.getPosition().y = userY;
                System.out.println("Movement received! " + "userId: " + userId  + " userX: " + userX + " userY: " + userY);
            }

            @Override
            public void messageReceived(int userId, String userMessage) {
                System.out.println("Message received! user_id: " + userId + " message: " + userMessage);
            }

            @Override
            public void throwReceived(int userId, float x, float y) {
                System.out.println("THROW RECEIVED! x: " + x + " y: "+ y);
            }

            @Override
            public void connectionError() {
                System.out.println("Connection error!");
            }

        });
    }
}
