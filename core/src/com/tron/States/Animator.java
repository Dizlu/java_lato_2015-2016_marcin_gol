package com.tron.states;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.tron.archiveFileHandle.ArchiveFileHandleResolver;

import java.util.zip.ZipFile;

/**
 * Created by Marcin on 13.05.2016.
 */
public class Animator implements ApplicationListener {

    private static final int FRAME_COLS = 3;
    private static final int FRAME_ROWS = 2;

    private Animation walkAnimation;
    private Texture walkSheet;
    private TextureRegion[]  walkFrames;
    private SpriteBatch spriteBatch;
    private TextureRegion currentFrame;
    private Vector2 position;
    private float stateTime;

    private ZipFile archive;
    private ArchiveFileHandleResolver resolver;
    private AssetManager assetManager;

    @Override
    public void create() {
        walkSheet = new Texture(Gdx.files.internal("captainAmerica.png"));
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);
        walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                walkFrames[index++] = tmp[i][j];
            }
        }
        walkAnimation = new Animation(0.045f, walkFrames);
        spriteBatch = new SpriteBatch();
        stateTime = 0f;
        position = new Vector2(0, 0);
    }

    public TextureRegion getCurrentFrame() {
        return currentFrame;
    }

    @Override
    public void render() {
        stateTime += Gdx.graphics.getDeltaTime();
        currentFrame = walkAnimation.getKeyFrame(stateTime, true);

    }

    @Override
    public void dispose(){
    }
    @Override
    public void resume(){
    }
    @Override
    public void pause(){
    }
    @Override
    public void resize(int x, int y){

    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }
}
