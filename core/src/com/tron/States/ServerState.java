package com.tron.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.tron.Tron;
import com.tron.javadb.FighterDB;
import com.tron.javadb.JavaDB;
import com.tron.socketApp.Client;
import com.tron.socketApp.Server;
import com.tron.sprites.Box;
import com.tron.sprites.Fighter;
import com.tron.utils.EUtil;

import java.io.IOException;
import java.util.Random;

/**
 * Created by Diylu on 29.03.2016.
 */
public class ServerState extends State{
    private Texture bg;
    private Server serverApp;
    private Server server;
    private Client client;
    private Vector2 position;
    private EUtil eventUtil;

    private FighterDB serverFighterDB = null;
    private JavaDB javaDB = null;
    private int rank;
    /****************************************** Including game mechanics */
    private static final int BOX_COUNT = 3;
    private static final int OFFSET = 120;
    private static final int WIDTH = Tron.WIDTH - 2 * OFFSET - 50;
    private static final int HEIGHT = Tron.HEIGHT - 70;
    private int rand1, rand2;
    private Fighter fighter, fighter2;
    private Boolean dirSwap;

    private Array<Box> boxes;
    private Random random;
    private String fighterLogin;
    /*****************/
    private Animator animation;

    ServerState(GameStateManager gsm, String l){
        super(gsm);
        animation = new Animator();
        animation.create();
        System.out.println("Witaj na serwerze, twoj login to: " + l + " !");
        javaDB = new JavaDB();
        fighterLogin = l;
        rank = Integer.parseInt(javaDB.szukajPrzyniesRank("fighters", "LOGIN", fighterLogin));
        System.out.println("Twoj rank to: " + rank);
        bg = new Texture("tronArena.png");
        cam.setToOrtho(false, Tron.WIDTH, Tron.HEIGHT);
        server = new Server();
        eventUtil = new EUtil();
        eventUtil.init(fighterLogin, server);
        client = new Client();
        position = new Vector2(100000, 12345678);


/*******************/
        fighter = new Fighter(2, fighterLogin, 700, 300);
        fighter2 = new Fighter(1, "Client", 50, 300);
        bg = new Texture("tronArena.png");
        cam.setToOrtho(false, Tron.WIDTH, Tron.HEIGHT);
        boxes = new Array<Box>();
        random = new Random();
        for(int i = 0; i < BOX_COUNT; i++){
            rand1 = random.nextInt(WIDTH / BOX_COUNT);
            rand2 = random.nextInt(HEIGHT / BOX_COUNT);
            boxes.add(new Box(OFFSET + i * WIDTH / BOX_COUNT + rand1, HEIGHT - i * HEIGHT / BOX_COUNT - rand2));
            rand1 = random.nextInt(WIDTH / BOX_COUNT);
            rand2 = random.nextInt(HEIGHT / BOX_COUNT);
            boxes.add(new Box(OFFSET + i * WIDTH / BOX_COUNT + rand1, i * HEIGHT / BOX_COUNT + rand2));
        }
        /*for (Box box: boxes){ // Moving not enough high boxes to the ground
            if(box.getPosBox().y < 100){
                box.getPosBox().y  = 0;
                box.updatePosBox();
            }
        }*/
        //safe
        try {
            connect();
        } catch (IOException e){}

    }

    @Override
    public void handleInput(){
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            gsm.set(new MenuState(gsm));
            dispose();
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.C)){

        }


        /************************************** */
        if (!server.isConnected()) {
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                fighter.move(1);
                //server.sendMovementFrame(1);
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
                fighter.move(2);
                //server.sendMovementFrame(2);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                fighter.move(3);
                //server.sendMovementFrame(3);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                fighter.move(4);
                //server.sendMovementFrame(4);
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.M)) {
                if (fighter.disc.attached) {
                    fighter.disc.toggleThrow();
                    server.sendMovementFrame(5);
                }
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            fighter.disc.toggleThrow();
            fighter2.disc.toggleThrow();
        }

        if(Gdx.input.isKeyPressed(Input.Keys.W)){
            fighter2.move(1);
            server.sendMovementFrame(1);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)){
            fighter2.move(2);
            server.sendMovementFrame(2);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)){
            fighter2.move(3);
            server.sendMovementFrame(3);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)){
            fighter2.move(4);
            server.sendMovementFrame(4);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.F)){
            if(fighter2.disc.attached) {
                fighter2.disc.toggleThrow();
                server.sendThrowFrame();
            }
        }
    }

    public void connect() throws IOException{
        eventUtil.init(fighterLogin, server);
        //safe
        if(server.start(8080, fighterLogin)){
            server.setClientFighter(fighter);
            server.setServerFighter(fighter2);
            server.setBoxes(boxes);
            System.out.println("We acomplished connection!");
        } else {
            System.out.println("Server connected!!");
        }
    }

    @Override
    public void update(float dt){
        handleInput();


        fighter.update(dt);
        fighter.disc.update(dt);
        fighter2.update(dt);
        fighter2.disc.update(dt);
        handleInput();
        for(Box box: boxes){
            if (box.collides(fighter.getBounds())) {
                handleBoxCollision(box, fighter);
            }
            if (box.collides(fighter2.getBounds())) {
                handleBoxCollision(box, fighter2);
            }
            if (fighter.collides(fighter2.disc.getBounds())){
                System.out.println("Kappa1");
                rank += 13;
                javaDB.zmien("fighters", "LOGIN", fighterLogin, "RANK", String.valueOf(rank));
                System.out.println("Twoj rank to: " + rank);
                //gsm.set(new MenuState(gsm));          // End of a game stack set
                //dispose();
            }
            if (fighter2.collides(fighter.disc.getBounds())){
                System.out.println("Kappa2");
                //gsm.set(new MenuState(gsm));
                //dispose();
                rank -= 13;
                javaDB.zmien("fighters", "LOGIN", fighterLogin, "RANK", String.valueOf(rank));
                System.out.println("Twoj rank to: " + rank);
            }
            dirSwap = false;
            /*Note to future self - only on direction change at a time*/
            if ((fighter.disc.collides(box.getBoundsRight(false)) || fighter.disc.collides(box.getBoundsRight(true))) // Ver. 1 of actual collision logic
                    && !fighter.disc.attached){
                fighter.disc.getVelocity().x = - fighter.disc.getVelocity().x;
                dirSwap = true;
            }
            if (fighter.disc.collides(box.getBoundsTop(false)) || fighter.disc.collides(box.getBoundsTop(true))
                    && !fighter.disc.attached && !dirSwap){
                fighter.disc.getVelocity().y = - fighter.disc.getVelocity().y;
            }
            dirSwap = false;
            if ((fighter2.disc.collides(box.getBoundsRight(false)) || fighter2.disc.collides(box.getBoundsRight(true))) // Ver. 1 of actual collision logic
                    && !fighter2.disc.attached){
                fighter2.disc.getVelocity().x = - fighter2.disc.getVelocity().x;
                dirSwap = true;
            }
            if (fighter2.disc.collides(box.getBoundsTop(false)) || fighter2.disc.collides(box.getBoundsTop(true))
                    && !fighter2.disc.attached && !dirSwap){
                fighter2.disc.getVelocity().y = - fighter2.disc.getVelocity().y;
            }
        }
    }

    @Override
    public void render(SpriteBatch sb){
        sb.setProjectionMatrix(cam.combined);


        sb.begin();
        sb.draw(bg, cam.position.x - (cam.viewportWidth / 2), 0);
        //sb.draw(fighter.getTexture(), fighter.getPosition().x, fighter.getPosition().y);
        fighter.animation.setPosition(fighter.getPosition().x, fighter.getPosition().y);
        fighter.animation.render();
        sb.draw(fighter.animation.getCurrentFrame(), fighter.getPosition().x, fighter.getPosition().y);

        fighter2.animation.setPosition(fighter2.getPosition().x, fighter2.getPosition().y);
        fighter2.animation.render();
        sb.draw(fighter2.animation.getCurrentFrame(), fighter2.getPosition().x, fighter2.getPosition().y);

        //sb.draw(fighter2.getTexture(), fighter2.getPosition().x, fighter2.getPosition().y);
        if (fighter2.disc.attached){
            sb.draw(fighter2.disc.getDisc(), fighter2.getPosition().x, fighter2.getPosition().y + 50);
        } else {
            sb.draw(fighter2.disc.getDisc(), fighter2.disc.getPosition().x, fighter2.disc.getPosition().y);
        }
        if (fighter.disc.attached){
            sb.draw(fighter.disc.getDisc(), fighter.getPosition().x, fighter.getPosition().y + 50);
        } else {
            sb.draw(fighter.disc.getDisc(), fighter.disc.getPosition().x, fighter.disc.getPosition().y);
        }
        for (Box box: boxes){
            sb.draw(box.getBox(), box.getPosBox().x, box.getPosBox().y);
        }

        sb.end();

    }

    @Override
    public void dispose(){
        bg.dispose();
        if (server.isConnected()){
            server.disconnect();
        }
    }




    public void handleBoxCollision(Box box, Fighter f){
        if ((f.getPosition().x + f.getTexture().getWidth()) > (box.getPosBox().x + 10) && f.getPosition().x < (box.getPosBox().x + box.getBox().getWidth() - 10)) {
            if (f.getPosition().y < box.getPosBox().y) {
                f.getPosition().y = box.getPosBox().y - f.getTexture().getHeight();
                if (f.getVelocity().y > 0){
                    f.getVelocity().y = 0;
                }
            } else if (f.getPosition().y > box.getPosBox().y) {
                f.getPosition().y = box.getPosBox().y + box.getBox().getHeight();
                f.setJumping();
                if (f.getVelocity().y < 0){
                    f.getVelocity().y = 0;
                }
            }
        }
        if ((f.getPosition().y + f.getTexture().getHeight()) > (box.getPosBox().y + 10) &&  f.getPosition().y  < (box.getPosBox().y + box.getBox().getHeight() - 10)) {
            if (f.getPosition().x < box.getPosBox().x) {
                f.getPosition().x = box.getPosBox().x - f.getTexture().getWidth();
            } else if (f.getPosition().x > box.getPosBox().x) {
                f.getPosition().x = box.getPosBox().x + box.getBox().getWidth();
            }
        }
    }
}

