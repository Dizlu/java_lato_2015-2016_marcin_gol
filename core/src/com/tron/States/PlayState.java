package com.tron.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.tron.Tron;
import com.tron.javadb.JavaDB;
import com.tron.socketApp.Client;
import com.tron.sprites.Box;
import com.tron.sprites.Disc;
import com.tron.sprites.Fighter;
import com.tron.utils.EUtil;

import java.io.IOException;
import java.util.Random;

/**
 * Created by Marcin on 06.03.2016.
 */
public class PlayState extends State {
    private static final int BOX_COUNT = 3;
    private static final int OFFSET = 120;
    private static final int WIDTH = Tron.WIDTH - 2 * OFFSET - 50;
    private static final int HEIGHT = Tron.HEIGHT - 70;
    private int rand1, rand2;
    private Fighter fighter, fighter2;
    private Texture bg;
    private boolean dirSwap;
    private Array<Box> boxes;
    private Random random;

    private Client client;
    private EUtil eventUtil;

    private String userLogin;
    private String fighterLogin;
    private JavaDB javaDB;
    private int rank;

    public PlayState(GameStateManager gsm, String l) {
        super(gsm);
        javaDB = new JavaDB();
        fighterLogin = l;
        rank = Integer.parseInt(javaDB.szukajPrzyniesRank("fighters", "LOGIN", fighterLogin));
        System.out.println("Twoj rank to: " + rank);
        System.out.println("Witaj na kliencie, twoj login to: " + l + " !");
        fighter = new Fighter(2, "Server", 700, 300);
        fighter2 = new Fighter(1, "Client", 50, 300);
        bg = new Texture("tronArena.png");
        cam.setToOrtho(false, Tron.WIDTH, Tron.HEIGHT);
        boxes = new Array<>();
        random = new Random();
        for(int i = 0; i < BOX_COUNT; i++){
            rand1 = random.nextInt(WIDTH / BOX_COUNT);
            rand2 = random.nextInt(HEIGHT / BOX_COUNT);
            boxes.add(new Box(OFFSET + i * WIDTH / BOX_COUNT + rand1, HEIGHT - i * HEIGHT / BOX_COUNT - rand2));
            rand1 = random.nextInt(WIDTH / BOX_COUNT);
            rand2 = random.nextInt(HEIGHT / BOX_COUNT);
            boxes.add(new Box(OFFSET + i * WIDTH / BOX_COUNT + rand1, i * HEIGHT / BOX_COUNT + rand2));
        }
        eventUtil = new EUtil();
        userLogin = "user010";
        client = new Client();
        try {
            connect();
        } catch (IOException e){

        }
        /*for (Box box: boxes){ // Moving not enough high boxes to the ground
            if(box.getPosBox().y < 100){
                box.getPosBox().y  = 0;
                box.updatePosBox();
            }
        }*/
        dirSwap = false;
    }


    @Override
    protected void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            fighter.move(1);
            client.sendMovementFrame(1);
        }if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            fighter.move(2);
            client.sendMovementFrame(2);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            fighter.move(3);
            client.sendMovementFrame(3);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            fighter.move(4);
            client.sendMovementFrame(4);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.M)){
            if(fighter.disc.attached) {
                fighter.disc.toggleThrow();
                client.sendThrowFrame();
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            fighter.disc.toggleThrow();
            fighter2.disc.toggleThrow();
        }

        if (!client.isConnected()) {
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                fighter2.move(1);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                fighter2.move(2);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                fighter2.move(3);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                fighter2.move(4);
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
                if (fighter2.disc.attached) {
                    fighter2.disc.toggleThrow();
                }
            }
        }
        if(Gdx.input.isKeyPressed(Input.Keys.C)){

        }
    }

    public void connect() throws IOException{
        eventUtil.init("Client", client);
        client.setClientFighter(fighter);
        client.setServerFighter(fighter2);
        //safe
        if(client.connect("127.0.0.1", 8080, userLogin)){
            System.out.println("Kappa such connect");

            client.sendMessageFrame("FAke");
            client.sendBoxesFrame(boxes);
            //client.runThread();
        } else
            System.out.println( "Server error!" );

    }

    @Override
    public void update(float dt) {

        fighter.update(dt);
        fighter.disc.update(dt);
        fighter2.update(dt);
        fighter2.disc.update(dt);
        handleInput();
        for(Box box: boxes){
            if (box.collides(fighter.getBounds())) {
                handleBoxCollision(box, fighter);
            }
            if (box.collides(fighter2.getBounds())) {
                handleBoxCollision(box, fighter2);
            }/*
            if (box.collides(fighter.disc.getBounds())) { // Old logic for box disc collision
                handleDiscCollision(box, fighter.disc);
            }
            if (box.collides(fighter.disc.getBounds())) {
                handleDiscCollision(box, fighter2.disc);
            }*/
            if (fighter.collides(fighter2.disc.getBounds())){
                rank -= 13;
                javaDB.zmien("fighters", "LOGIN", fighterLogin, "RANK", String.valueOf(rank));
                System.out.println("Twoj rank to: " + rank);
                //gsm.set(new MenuState(gsm));          // End of a game stack set
                //dispose();
                System.out.println("Kappa1");

            }
            if (fighter2.collides(fighter.disc.getBounds())){
                rank += 13;
                javaDB.zmien("fighters", "LOGIN", fighterLogin, "RANK", String.valueOf(rank));
                System.out.println("Twoj rank to: " + rank);

                System.out.println("Kappa2");
                //gsm.set(new MenuState(gsm));
                //dispose();
            }
            /*Note to future self - only on direction change at a time*/
            if ((fighter.disc.collides(box.getBoundsRight(false)) || fighter.disc.collides(box.getBoundsRight(true))) // Ver. 1 of actual collision logic
                     && !fighter.disc.attached){
                fighter.disc.getVelocity().x = - fighter.disc.getVelocity().x;
                dirSwap = true;
            }
            if (fighter.disc.collides(box.getBoundsTop(false)) || fighter.disc.collides(box.getBoundsTop(true))
                    && !fighter.disc.attached && !dirSwap){
                fighter.disc.getVelocity().y = - fighter.disc.getVelocity().y;
            }
            dirSwap = false;
            if ((fighter2.disc.collides(box.getBoundsRight(false)) || fighter2.disc.collides(box.getBoundsRight(true))) // Ver. 1 of actual collision logic
                    && !fighter2.disc.attached){
                fighter2.disc.getVelocity().x = - fighter2.disc.getVelocity().x;
                dirSwap = true;
            }
            if (fighter2.disc.collides(box.getBoundsTop(false)) || fighter2.disc.collides(box.getBoundsTop(true))
                    && !fighter2.disc.attached && !dirSwap){
                fighter2.disc.getVelocity().y = - fighter2.disc.getVelocity().y;
            }
            /*
//          FLIGHT OF THE BUMBLEBEE FFS
            if (fighter.disc.collisionMathSides(box.getBoundsRight(false)) || fighter.disc.collisionMathSides(box.getBoundsRight(false))
                    && !fighter.disc.attached){
                fighter.disc.getVelocity().x = - fighter.disc.getVelocity().x;
            }
            if (fighter.disc.collisionMathTop(box.getBoundsTop(false)) || fighter.disc.collisionMathTop(box.getBoundsTop(false))
                    && !fighter.disc.attached){
                fighter.disc.getVelocity().y = - fighter.disc.getVelocity().y;

            }
            }*/
            dirSwap = false;


            //client.runThread();
        }
    }


    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg, cam.position.x - (cam.viewportWidth / 2), 0);

        sb.draw(fighter2.getTexture(), fighter2.getPosition().x, fighter2.getPosition().y);
        sb.draw(fighter.getTexture(), fighter.getPosition().x, fighter.getPosition().y);
        if (fighter2.disc.attached){
            sb.draw(fighter2.disc.getDisc(), fighter2.getPosition().x, fighter2.getPosition().y + 50);
        } else {
            sb.draw(fighter2.disc.getDisc(), fighter2.disc.getPosition().x, fighter2.disc.getPosition().y);
        }
        if (fighter.disc.attached){
            sb.draw(fighter.disc.getDisc(), fighter.getPosition().x, fighter.getPosition().y + 50);
        } else {
            sb.draw(fighter.disc.getDisc(), fighter.disc.getPosition().x, fighter.disc.getPosition().y);
        }
        for (Box box: boxes){
            sb.draw(box.getBox(), box.getPosBox().x, box.getPosBox().y);
        }
        sb.end();
    }

    @Override
    public void dispose() {
        //safe
        if(client.isConnected()){
            try {
                client.sendLogoutFrame();
            } catch (IOException e ){
                System.out.println("Problem while logging out!");
            }
            client.disconnect();
        }
        bg.dispose();
    }

    public void handleBoxCollision(Box box, Fighter f){
        if ((f.getPosition().x + f.getTexture().getWidth()) > (box.getPosBox().x + 10) && f.getPosition().x < (box.getPosBox().x + box.getBox().getWidth() - 10)) {
            if (f.getPosition().y < box.getPosBox().y) {
                f.getPosition().y = box.getPosBox().y - f.getTexture().getHeight();
                if (f.getVelocity().y > 0){
                    f.getVelocity().y = 0;
                }
            } else if (f.getPosition().y > box.getPosBox().y) {
                f.getPosition().y = box.getPosBox().y + box.getBox().getHeight();
                f.setJumping();
                if (f.getVelocity().y < 0){
                    f.getVelocity().y = 0;
                }
            }
        }
        if ((f.getPosition().y + f.getTexture().getHeight()) > (box.getPosBox().y + 10) &&  f.getPosition().y  < (box.getPosBox().y + box.getBox().getHeight() - 10)) {
            if (f.getPosition().x < box.getPosBox().x) {
                f.getPosition().x = box.getPosBox().x - f.getTexture().getWidth();
            } else if (f.getPosition().x > box.getPosBox().x) {
                f.getPosition().x = box.getPosBox().x + box.getBox().getWidth();
            }
        }
    }


    public void handleFighterDiscCollision(Fighter f, Disc d){
        if ((d.getPosition().x + d.getDisc().getWidth()) > f.getPosition().x && d.getPosition().x < (f.getPosition().x + d.getDisc().getWidth())) {
            System.out.println("KAppa");
        }
        if ((d.getPosition().y + d.getDisc().getHeight()) > f.getPosition().y &&  d.getPosition().y  < (f.getPosition().y + d.getDisc().getHeight())){
            System.out.println("Kappa");
        }
    }
}
