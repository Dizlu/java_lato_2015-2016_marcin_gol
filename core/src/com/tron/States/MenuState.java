package com.tron.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tron.Tron;
import com.tron.VirtualFileSystem.ZipFSPUser;
import com.tron.archiveFileHandle.ArchiveFileHandle;
import com.tron.archiveFileHandle.ArchiveFileHandleResolver;
import com.tron.javadb.FighterDB;
import com.tron.javadb.JavaDB;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.zip.ZipFile;

/**
 * Created by Marcin on 06.03.2016.
 */
public class MenuState extends State {
    private Texture background;
    private Texture playBtn;
    //private Texture serverBtn;

    private Stage stage;
    private Skin skin;
    private JavaDB javaDB;
    private int count;
    private String fighterLogin;
    private Random rand = new Random();

    private ZipFSPUser zip;
    private ZipFile archive;
    private ArchiveFileHandleResolver resolver;
    private AssetManager assetManager;



    public MenuState(GameStateManager gsm) {
        super(gsm);


        /**Virtual File System*************************************/
        try {
            zip = new ZipFSPUser();
            //System.out.println(zip.SOURCE_FOLDER + "\n" + zip.OUTPUT_ZIP_FILE);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        /**************************************/

        count = rand.nextInt(1000000);
        playBtn = new Texture("play.png");
        //serverBtn = new Texture("play.png");
        fighterLogin = "New Player";

        skin = new Skin(Gdx.files.internal("uiskin.json"));
        stage = new Stage();
        final TextButton button = new TextButton("Click me", skin, "default");
        final TextButton serverButton = new TextButton("Host a game", skin, "default");
        final TextButton clientButton = new TextButton("Join a game", skin, "default");
        final TextButton registerButton = new TextButton("Register", skin, "default");
        final TextButton register = new TextButton("Register here", skin, "default");
        final Label loginLabel = new Label("Login", skin, "default");
        final Label passwordLabel = new Label("Password", skin, "default");
        final TextField loginTextField = new TextField("", skin, "default");
        final TextField passwordTextField = new TextField("", skin, "default");
        final Table table = new Table();
        javaDB = new JavaDB();

        final TextButton singlePlayer = new TextButton("Single Player", skin, "default");



        button.setSize(200, 40);
        serverButton.setSize(200, 40);
        clientButton.setSize(200, 40);
        serverButton.setPosition((Tron.WIDTH / 2) - (serverButton.getWidth() / 2) - 200, Tron.HEIGHT / 2 );
        clientButton.setPosition((Tron.WIDTH / 2) - (clientButton.getWidth() / 2) - 200, Tron.HEIGHT / 2 - 150 );
        button.setPosition(500, 100);

        loginTextField.setSize(200, 40);
        loginTextField.setPosition(500, 300);
        loginLabel.setPosition(500, 360);
        loginTextField.setFocusTraversal(true);
        passwordTextField.setSize(200, 40);
        passwordTextField.setPosition(500, 200);
        passwordLabel.setPosition(500, 260);
        passwordTextField.setPasswordMode(true);
        passwordTextField.setPasswordCharacter('*');
        registerButton.setSize(150, 35);
        registerButton.setPosition(550, 30);


        singlePlayer.setSize(200, 40);
        singlePlayer.setPosition((Tron.WIDTH / 2) - (singlePlayer.getWidth() / 2) - 200, Tron.HEIGHT / 2 - 60 );

        registerButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                register.setSize(200, 40);
                register.setPosition(500, 60);
                button.setPosition(5000, 6000);
                stage.addActor(register);
                registerButton.setPosition(5000, 6000);
            }
        });

        register.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                FighterDB fighter1 = new FighterDB(count,
                        loginTextField.getText(),
                        passwordTextField.getText(),
                        30, 40, 10, 3, "fighter.png", 0);
                count = rand.nextInt(10000);
                fighterLogin = loginTextField.getText();
                System.out.println("Login to: " + fighterLogin + " !!!!!!!!");
                javaDB.dodajDane(fighter1, "fighters");
                register.setPosition(5000, 6000);
                button.setPosition(500, 100);

                loginTextField.setPosition(5000, 3000);
                loginLabel.setPosition(5000, 3600);
                passwordTextField.setPosition(50000, 2000);
                passwordLabel.setPosition(5000, 2600);
                button.setPosition(50000, 50000);

                Map entries = javaDB.fetch("fighters", "RANK", "ASC");
                Iterator it = entries.entrySet().iterator();
                while (it.hasNext()){
                    Map.Entry pair = (Map.Entry)it.next();
                    Label label1 = new Label(pair.getKey().toString(), skin);
                    Label label2 = new Label(pair.getValue().toString(), skin);
                    table.add(label1).width(200);
                    table.add(label2).width(100);
                    table.row();
                    it.remove();
                }

                table.setPosition(550, 260);
            }
        });


        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                System.out.println("Login: " + loginTextField.getText() + " Password: " + passwordTextField.getText());
                if (!loginTextField.getText().isEmpty() && !passwordTextField.getText().isEmpty()) {
                    if (passwordTextField.getText() != javaDB.szukajPrzyniesPassword("fighters", "LOGIN", loginTextField.getText())){
                        System.out.println("Jego haslo to: " + javaDB.szukajPrzyniesPassword("fighters", "LOGIN", loginTextField.getText())+ " !!!!!!!!!!!!!!");
                        System.out.println(passwordTextField.getText() + " A TO HASLO WPISANE!!!");
                        return;
                    }
                    // Walidacja hasła
                    fighterLogin = javaDB.szukajPrzynies("fighters", "LOGIN", loginTextField.getText());



                    FighterDB fighter1 = new FighterDB(count, loginTextField.getText(), passwordTextField.getText(), 30, 40, 10, 3, "fighter.png", 0);
                    if (fighterLogin != null) {
                    fighter1.setRank(Integer.parseInt(javaDB.szukajPrzyniesRank("fighters", "LOGIN", loginTextField.getText())));
                    System.out.println(fighter1.getRank() + " <---------- rank !!");

                        System.out.println("Login jesli byl w bazie: " + fighterLogin);
                        count = rand.nextInt(1000000);

                        loginTextField.setPosition(5000, 3000);
                        loginLabel.setPosition(5000, 3600);
                        passwordTextField.setPosition(50000, 2000);
                        passwordLabel.setPosition(5000, 2600);
                        button.setPosition(50000, 50000);

                        Map entries = javaDB.fetch("fighters", "RANK", "ASC");
                        Iterator it = entries.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            Label label1 = new Label(pair.getKey().toString(), skin);
                            Label label2 = new Label(pair.getValue().toString(), skin);
                            table.add(label1).width(200);
                            table.add(label2).width(100);
                            table.row();
                            it.remove();
                        }

                        table.setPosition(550, 260);
                    } else {
                        System.out.println("Blad przy logowaniu!");
                        button.setText("Wpisz poprawne dane!");
                    }
                } else {
                    button.setText("Uzupelnij dane!");
                    System.out.println("Podaj prawidlowe dane!");
                }
            }
        });
        serverButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new ServerState(gsm, fighterLogin));
                dispose();
            }
        });
        clientButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new PlayState(gsm, fighterLogin));
                dispose();
            }
        });

        singlePlayer.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y){
                gsm.set(new SingleState(gsm, fighterLogin));
                dispose();
            }
        });

        stage.addActor(button);
        stage.addActor(serverButton);
        stage.addActor(clientButton);
        stage.addActor(loginTextField);
        stage.addActor(loginLabel);
        stage.addActor(passwordTextField);
        stage.addActor(passwordLabel);
        stage.addActor(table);
        stage.addActor(registerButton);
        stage.addActor(singlePlayer);
        Gdx.input.setInputProcessor(stage);
        //background = new Texture("tronArena.png");

        /**Virtual File System 2************************************/
        try {
            archive = new ZipFile(Gdx.files.internal("zip/newZip.zip").file());
            resolver = new ArchiveFileHandleResolver(archive);
            assetManager = new AssetManager(resolver);
            assetManager.setLoader(Texture.class, new TextureLoader(resolver));
            assetManager.load("tronArena.PNG", Texture.class);
            assetManager.setLoader(Music.class, new MusicLoader(resolver));
            assetManager.load("rinzler.mp3", Music.class);
            assetManager.finishLoading();
            background = assetManager.get("tronArena.PNG");
            Music music = assetManager.get("rinzler.mp3");


            //Music music = Gdx.audio.newMusic(Gdx.files.internal("rinzler.mp3"));
            music.setVolume(0.5f);
            music.setLooping(true);
            music.play();
            /*ZipFile archive = new ZipFile(Gdx.files.internal("test.zip").file());
            ArchiveFileHandleResolver resolver = new ArchiveFileHandleResolver(archive);
            assetManager = new AssetManager(resolver);
            assetManager.setLoader(TiledMap.class, new TmxMapLoader(resolver));
            assetManager.load("tmx/level1.tmx", TiledMap.class);
            assetManager.finishLoading();
            map = assetManager.get("tmx/level1.tmx");*/
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        /**************************************/


    }



    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void handleInput() {
        /*if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            gsm.set(new PlayState(gsm));
            dispose();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)){
            gsm.set(new ServerState(gsm));
            dispose();
        }*/

    }

    @Override
    public void render(SpriteBatch sb) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0, 0, Tron.WIDTH, Tron.HEIGHT);
        sb.draw(playBtn, (Tron.WIDTH / 2) - (playBtn.getWidth() / 2), Tron.HEIGHT / 2 );
        //sb.draw(serverBtn, (Tron.WIDTH / 2) - (playBtn.getWidth() / 2), Tron.HEIGHT / 2 - 150 );


        stage.draw();
        sb.end();
    }
    @Override
    public void dispose() {
        background.dispose();
        playBtn.dispose();
        //serverBtn.dispose();
    }
}
