package com.tron.archiveFileHandle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;
/**
 * Created by Marcin on 18.05.2016.
 */
public class ArchiveFileHandle extends FileHandle {
    final ZipFile archive;
    final ZipEntry archiveEntry;

    public ArchiveFileHandle (ZipFile archive, File file) {
        super(file, Files.FileType.Classpath);
        this.archive = archive;
        archiveEntry = this.archive.getEntry(file.getPath());
    }

    public ArchiveFileHandle (ZipFile archive, String fileName) {
        super(fileName.replace('\\', '/'), Files.FileType.Classpath);
        this.archive = archive;
        this.archiveEntry = archive.getEntry(fileName.replace('\\', '/'));
    }

    @Override
    public InputStream read () {
        try {
            return archive.getInputStream(archiveEntry);
        } catch (IOException e) {
            throw new GdxRuntimeException("File not found: " + file + " (Archive)");
        }
    }

    @Override
    public boolean exists() {
        return archiveEntry != null;
    }

    @Override
    public long length () {
        return archiveEntry.getSize();
    }

    @Override
    public long lastModified () {
        return archiveEntry.getTime();
    }
}
