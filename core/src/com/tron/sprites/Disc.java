package com.tron.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.tron.Tron;

/**
 * Created by Marcin on 14.03.2016.
 */
public class Disc {
    private static final int SPEED = 15;

    private Vector2 position;
    private Vector2 velocity;
    private Vector2 positionNext;
    private Texture Disc;

    public Boolean attached;
    private Rectangle bounds;


    public Disc(){
        position = new Vector2();
        positionNext = new Vector2();
        velocity = new Vector2();

        Disc = new Texture("disc.png");
        attached = true;
        bounds = new Rectangle(position.x, position.y, Disc.getWidth(), Disc.getHeight());
    }

    public void update(float dt){
        positionNext = position;
        velocity.scl(dt);
        position.add(velocity);
        positionNext.add(velocity.x * 2 * dt, velocity.y * 2 * dt);
        if (position.x < 0){
            velocity.x = -velocity.x;
        }
        if (position.x > Tron.WIDTH){
            velocity.x = -velocity.x;
        }
        if (position.y < 0){
            velocity.y = -velocity.y;
        }
        if (position.y > Tron.HEIGHT){
            velocity.y = - velocity.y;
        }
        if (position.x == 0){
            velocity.x = -SPEED;
        }
        if (position.y == 0){
            velocity.y = -SPEED;
        }

        velocity.scl(1/dt);
        bounds.setPosition(position.x, position.y);
    }

    public Texture getDisc() {
        return Disc;
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void toggleThrow(){
        attached = !attached;
    }

    public Rectangle getBounds(){
        return bounds;
    }

    public Boolean collides(Rectangle r) { return r.overlaps(bounds); }

    public Boolean collisionMathTop(Rectangle r){
        float diffx = (position.x - positionNext.x);
            return position.x >= r.getX() && (position.x + diffx) <= (r.getX() + r.getWidth()) ? true : false;
    }
    public Boolean collisionMathSides(Rectangle r){
        float diffy = position.y - positionNext.y;
            return position.y >= r.getY() && (position.y + diffy) <= (r.getY() + r.getHeight()) ? true : false;
    }
}
