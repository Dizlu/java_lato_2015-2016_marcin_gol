package com.tron.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.sun.org.apache.regexp.internal.ReaderCharacterIterator;
import com.tron.Tron;

import java.util.Random;

/**
 * Created by Marcin on 14.03.2016.
 */public class Box {
    private static final int FLUCTUATION = 570;
    private static final int PLAYER_GAP = 100;
    public Texture Box;
    private Vector2 posBox;
    private Rectangle bounds;
    private Rectangle rectTop, rectBot, rectLeft, rectRight;


    public Box(float x, float y) {
        Box = new Texture("botBox.png");
        posBox = new Vector2(x, y);
        bounds = new Rectangle(posBox.x, posBox.y, Box.getWidth(), Box.getHeight());
        rectBot = new Rectangle(posBox.x + 5, posBox.y, Box.getWidth() - 10, 10); // Dol
        rectTop = new Rectangle(posBox.x + 5, posBox.y + Box.getHeight() - 10, Box.getWidth() - 10, 10); // Gora
        rectRight = new Rectangle(posBox.x + Box.getWidth() - 10, posBox.y, 10, Box.getHeight()); //Prawo
        rectLeft = new Rectangle(posBox.x, posBox.y, 10, Box.getHeight()); // Lewo
    }


    public void updateBounds(){
        rectBot.setPosition(posBox.x + 5, posBox.y); // Dol
        rectTop.setPosition(posBox.x + 5, posBox.y + Box.getHeight() - 10); // Gora
        rectRight.setPosition(posBox.x + Box.getWidth() - 10, posBox.y); //Prawo
        rectLeft.setPosition(posBox.x, posBox.y); // Lewo
    }

    public Rectangle getBounds(){
        return bounds;
    }

    public Texture getBox() {
        return Box;
    }

    public Vector2 getPosBox() {
        return posBox;
    }

    public Boolean collides(Rectangle fighter) {
        return fighter.overlaps(bounds);
    }

    public Rectangle getBoundsTop(Boolean choice) { return choice ? rectTop : rectBot; }

    public Rectangle getBoundsRight(Boolean choice) { return choice ? rectRight : rectLeft; }

    public void updatePosBox(){
        rectBot.setPosition(posBox.x + 5, posBox.y);
        rectTop.setPosition(posBox.x + 5, posBox.y + Box.getHeight());
        rectRight.setPosition(posBox.x + Box.getWidth() - 10, posBox.y);
        rectLeft.setPosition(posBox.x, posBox.y);
    }
}