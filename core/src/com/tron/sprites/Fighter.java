package com.tron.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.tron.Tron;
import com.tron.states.Animator;

/**
 * Created by Marcin on 07.03.2016.
 */
public class Fighter {
    private static final int GRAVITY = -15;
    private static final int FRICTION = -30;
    private Boolean jumping, doubleJumping;
    private Vector2 position;
    private Vector2 velocity;
    public int id;
    private String login;
    private Texture fighter;
    public Rectangle bounds;
    private TextureRegion currentFrame;

    public Animator animation;
    public Disc disc;

    public Fighter(int id, String login, int x, int y){
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        fighter = new Texture("rinzler.png");
        bounds = new Rectangle(x, y, fighter.getWidth(), fighter.getHeight());
        disc = new Disc();
        this.id = id;
        this.login = login;
        jumping = false;
        doubleJumping = false;
        animation = new Animator();
        animation.create();
    }

    public Fighter copyFighter(Fighter fighter){
        Fighter f = new Fighter(0, "", 0, 0);
        f.position = fighter.getPosition();
        f. velocity = fighter.getVelocity();
        f.fighter = fighter.fighter;
        f.bounds = fighter.getBounds();
        f.disc = fighter.disc;
        f.id = fighter.id;
        f.login = fighter.login;
        f.jumping = fighter.jumping;
        f.doubleJumping = fighter.doubleJumping;
        return f;
    }
    public void update(float dt){
        /****************************** Basic collisons with boundaries of game box */
        currentFrame = animation.getCurrentFrame();


        if (position.y > 0) {
            velocity.add(0, GRAVITY);
        }
        if (position.x > 0 && position.x < Tron.WIDTH - 50){
            if (velocity.x > 0){
                velocity.add(FRICTION, 0);
            } else if(velocity.x > -30 && velocity.x < 30){
                velocity.x = 0;
            }
            else {
                velocity.add(-FRICTION, 0);
            }
        }
        velocity.scl(dt);
        position.add(velocity);

        if(position.y < 0){
            position.y = 0;
            jumping = false;
            doubleJumping = false;
        } else if(position.y > Tron.HEIGHT - 68){
            position.y = Tron.HEIGHT - 68;
            velocity.y = 0;
        }
        if (position.x < 0 || position.x > Tron.WIDTH - 50){
            if (position.x < 0){
                position.x = 0;
            } else {
                position.x = Tron.WIDTH - 50;
            }
            jumping = false;
        }
        /*************************************** Logic for disc throw and catching him */

        if(disc.attached){
            if (velocity.x > 0) {
                disc.getPosition().x = position.x + 70;
                disc.getPosition().y = position.y + 80;
            } else if(velocity.y < 0) {
                disc.getPosition().y = position.y + 70;
                disc.getPosition().x = position.x;
            } else {
                disc.getPosition().x = position.x;
                disc.getPosition().y = position.y;
            }
            disc.getVelocity().x = velocity.x * 35;
            disc.getVelocity().y = velocity.y * 35;
        }
        if (disc.getPosition().y < -10){
            disc.getPosition().y = 0;
        }


        if (disc.getPosition().x >= getPosition().x && (disc.getPosition().x <= (getPosition().x + getTexture().getWidth()))){
            if (disc.getPosition().y >= getPosition().y && (disc.getPosition().y <= (getPosition().y + getTexture().getHeight()))){
                disc.attached = true;
            }
        }
        velocity.scl(1/dt);
        disc.update(dt);
        bounds.setPosition(position.x, position.y);
        animation.setPosition(position.x, position.y);
    }


    public Vector2 getPosition() {
        return position;
    }
    public Texture getTexture() {
        return fighter;
    }
    public Vector2 getVelocity() {
        return velocity;
    }
    public Rectangle getBounds(){
        return bounds;
    }
    public int getId(){ return id; }
    public String getLogin() { return this.login; }
    public Disc getDisc() {
        return disc;
    }
    public TextureRegion getCurrentFrame() { return currentFrame; }

    public void move(int d){
        if(d == 1){
            if (!jumping && !doubleJumping){
                velocity.y = 500;
                jumping = true;
            }
        } else if (d == 2){
            velocity.y = -500;
        } else if (d == 3){
            velocity.x = -450;
        } else if (d == 4){
            velocity.x = 450;
        }
    }

    public void setJumping(){
        jumping = false;
    }
    public void setVelocity(Vector2 vel){ this.velocity = vel; }
    public Boolean collides(Rectangle disc) {
        return disc.overlaps(bounds);
    }
}
