package com.tron.VirtualFileSystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Marcin on 17.05.2016.
 */
public class ZipFSPUser {
    byte[] buffer = new byte[1024];

    public ZipFSPUser() {
        try {

            //C:\Users\Marcin\Desktop\intelijProjects\tron\android\assets\zip\newZip.zip
            //C:\Users\Marcin\Desktop\intelijProjects\tron\out\production\core\com\tron\VirtualFileSystem\ZipFSPUser.class
            //C:\Users\Marcin\Desktop\intelijProjects\tron\core\assets1\zip\newZip.zip
            //C:\Users\Marcin\Desktop\intelijProjects\tron\out\production\core\com\tron\VirtualFileSystem\assets1\zip\newZip.zip
            String filePath = ZipFSPUser.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            System.out.println(filePath);
            FileOutputStream fos = new FileOutputStream(filePath + "\\com\\tron\\VirtualFileSystem\\assets1\\zip\\newZip.zip");

            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry ze = new ZipEntry("tronArena.PNG");
            zos.putNextEntry(ze);
            FileInputStream in = new FileInputStream(filePath + "\\com\\tron\\VirtualFileSystem\\assets1\\sourceFolder\\tronArena.PNG");
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();

            ze = new ZipEntry("rinzler.mp3");
            zos.putNextEntry(ze);
            in = new FileInputStream(filePath + "\\com\\tron\\VirtualFileSystem\\assets1\\sourceFolder\\rinzler.mp3");
            /************************************************************** */
            /*FileOutputStream fos = new FileOutputStream("C:\\Users\\Marcin\\Desktop\\intelijProjects\\tron\\android\\assets\\zip\\newZip.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry ze = new ZipEntry("tronArena.PNG");
            zos.putNextEntry(ze);
            FileInputStream in = new FileInputStream("C:\\Users\\Marcin\\Desktop\\intelijProjects\\tron\\android\\assets\\sourceFolder\\tronArena.PNG");

            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();

            ze = new ZipEntry("rinzler.mp3");
            zos.putNextEntry(ze);
            in = new FileInputStream("C:\\Users\\Marcin\\Desktop\\intelijProjects\\tron\\android\\assets\\sourceFolder\\rinzler.mp3");
*/
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();

            //remember close it
            zos.close();

            System.out.println("Done");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
